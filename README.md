# What you'll need?

You need to install **NodeJS** to run the code and install dependencies and also **Postman** to test the API.


# Challenge
There's a little challenge for you guys in the *controller.js* file. 

It's a small task to help you get a better understanding of APIs, write a bit of JS, look into Express's documnentation (*looking stuff on the Internet <-- developer's #1 job*) and work with GIT.


Branch out from **master** and work in your own branch. When you think you're ready, push your work to the remote repo (*again, on your branch <-- **don't touch master***)


## Give me a shout, if you have any questions!