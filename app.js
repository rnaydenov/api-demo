const express = require('express');
const bodyParser = require('body-parser');
const setupController = require('./controller');
const app = express();

const port = process.env.PORT || 8900;

app.use(bodyParser.json())
setupController(app);

app.listen(port);