const idMachine = require('shortid');

module.exports = app => {

    // The graduates data
    const data = [
        {   
            id: "nYrnfYEv",
            name: "Kiki",
            age: 23,
            favMusic: "jazz"
        },
        {   
            id: "a4vhAoFG",
            name: "Jan",
            age: 22,
            favMusic: "techno"
        },
        {   
            id: "hwX6aOr7",
            name: "Sammy",
            age: 23,
            favMusic: "rock"
        }
    ]


    // GET requests accessing root "/"  
    app.get('/', (req, res) => {
        res.send("Hello from the API!!")
    }); 



    // GET requests for resource at "/grads"  
    app.get('/', (req, res) => {
        
        // Return JSON representation of data above
        res.send(JSON.stringify(data));
    }); 


    // GET requests for adding a new grad resource at "/grads"      
    app.get('/grads', (req, res) => {
        const { name } = req.query;
        
        // If we specify a name query parameter
        // return an array of all the people that have that name
        // e.g. http://localhost:8900/grads?name=Sammy
        if(name){
            const personWithName = data.filter(person => person.name === name)
            res.send(JSON.stringify(personWithName));
        }

        res.send(JSON.stringify(data));
    }); 

    // POST requests for adding a new grad resource at "/grads"  
    app.post('/grads', (req,res) => {
        // Generating a random id
        const id = idMachine.generate();

        // req.body - incoming POST request data representing the new grad
        // idMachine.generate() - generates an id
        // { id: idMachine.generate(), ...req.body } <-- this fancy thing
        //                                       puts all the properties in a single object
        const newGrad = { id: idMachine.generate(), ...req.body };

        // Updating our data
        data.push(newGrad);

        // Returning a 201 status code as a response 
        // along with the newGrad data
        res.status(201).send(JSON.stringify(newGrad));
    }); 



     /**
      * CHALLENGE:
      * GET requests for resource with specified id at "/grads/:id
      * 
      * Complete the code so that the API returns the data for a grad with a specified id
      * 
      * e.g. We do a GET request to http://localhost:8900/grads/hwX6aOr7
      *      and we receive the JSON data for Sammy 
      */  
     app.get('endpoint to complete', (req, res) => {
        // TODO: Code to complete
    }); 

}
